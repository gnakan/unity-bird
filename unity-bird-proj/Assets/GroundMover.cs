﻿using UnityEngine;
using System.Collections;

public class GroundMover : MonoBehaviour {

	//float speed = 0f;
	Rigidbody2D player;


	void Start(){
		GameObject player_go = GameObject.FindGameObjectWithTag ("Player");

		if (player_go == null) {
			return;
		}

		player = player_go.rigidbody2D;
	}

	void FixedUpdate(){
		//Vector3 pos = transform.position;
		//pos.x += speed * Time.deltaTime;
		//transform.position = pos;
		float vel = player.velocity.x;
		transform.position = transform.position + Vector3.right * vel * Time.deltaTime;
	}
}
