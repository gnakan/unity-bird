﻿using UnityEngine;
using System.Collections;

public class BGLooper : MonoBehaviour {

	int numBGPanels = 6; //Number of background image tiles

	float pipeMax = 1.369756f;
	float pipeMin = 0.6880636f;

	void Start(){
		//randomize up/down position of pipes
		//TODO: Move this to another script to maintain 
		GameObject[] pipes = GameObject.FindGameObjectsWithTag ("Pipe");

		foreach (GameObject pipe in pipes) {
			Vector3 pos = pipe.transform.position;
			pos.y = Random.Range(pipeMin, pipeMax);
			pipe.transform.position = pos;
		}
	}



	void OnTriggerEnter2D(Collider2D collider){

		//looping the background
		float widthOfBGObject = ((BoxCollider2D)collider).size.x;
		Vector3 pos = collider.transform.position;
		pos.x += widthOfBGObject * numBGPanels; //helps adjust the spacing the background

		//change the up/down position of pipes
		if (collider.tag == "Pipe") {
			pos.y = Random.Range(pipeMin, pipeMax);
		}


		collider.transform.position = pos;


	}
}
